module.exports = {
  url: function(){
    return false;
  },
  elements: {
    getLocationBtn: {
        selector: "#getLocation"
    },
    getLocationDebug: {
      selector: "#locationDebug"
    },
    goBtn: {
      selector: "#goAddon"
    },
    reloadTestInput: {
      selector: "input[id=reloadTest]"
    },
    reloadAddon: {
      selector: "#reloadAddon"
    }
  }
};